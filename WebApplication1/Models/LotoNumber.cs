﻿using System;

namespace WebApplication1.Models
{
    public class LotoNumber
    {
        public int Index { get; set; }
        public bool Enabled { get; set; } = true;
        public string StringValue => Index.ToString();
        public Func<LotoNumber,Microsoft.AspNetCore.Mvc.ActionResult> ActionOnClick { get; set; }
        public string Color { get; set; }
    }
}
